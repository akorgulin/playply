<?php
IncludeModuleLangFile(__FILE__);

use \Bitrix\Main\ModuleManager;

Class iblocklog extends CModule
{
    var $MODULE_ID = "iblocklog";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function iblocklog()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Логирование инфоблоков";
        $this->MODULE_DESCRIPTION = "Логирование инфоблоков";
    }
	
	function InstallDB()
    {
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;

        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler(
            'iblock',
            'OnAfterIBlockElementAdd',
            $this->MODULE_ID,
            'Iblocklog\D7\Handlers',
            'handlerIblockElementAdd'
        );

		// Database tables creation
		$rs = $DB->Query("SELECT * FROM information_schema.tables WHERE table_name = 'c_iblock_element_log' LIMIT 1", true);
		if(!$row = $rs->Fetch()) 
		{ 
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/iblocklog/install/db/".strtolower($DB->type)."/install.sql");
		}

		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}
		else
		{
			RegisterModule($this->MODULE_ID);
			CModule::IncludeModule($this->MODULE_ID);
			
			return true;
		}
    }
	
	function UnInstallDB()
    {
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/iblocklog/install/db/".strtolower($DB->type)."/uninstall.sql");
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->unregisterEventHandler(
            'iblock',
            'OnAfterIBlockElementAdd',
            $this->MODULE_ID,
            'Iblocklog\D7\Handlers',
            'handlerIblockElementAdd'
        );
        UnRegisterModule($this->MODULE_ID);
		
		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}

        return true;
    }
	
	
	function InstallFiles()
    {
		mkdir($_SERVER["DOCUMENT_ROOT"]."/local/modules/iblocklog/errors/", 0755);
        return true;
    }

    function UnInstallFiles()
    {
		
		$dir = $_SERVER["DOCUMENT_ROOT"]."/local/modules/iblocklog/errors";
		if ($objs = glob($dir."/*")) {
			foreach($objs as $obj) {
			is_dir($obj) ? removeDirectory($obj) : unlink($obj);
			}
		}

        return true;
    }

    function DoInstall()
    {
        global $DB, $DOCUMENT_ROOT, $APPLICATION;
        // Install events, database structure, files

        if (!IsModuleInstalled($this->MODULE_ID))
        {
            $this->InstallDB();
            $this->InstallFiles();
			$APPLICATION->IncludeAdminFile("Установка модуля 'Логирование инфоблоков'", $DOCUMENT_ROOT."/local/modules/iblocklog/install/step.php");
        }
        return true;
    }

    function DoUninstall()
    {
        global $DB, $DOCUMENT_ROOT, $APPLICATION;
        
		$this->UnInstallDB();
        $this->UnInstallFiles();
        $APPLICATION->IncludeAdminFile("Деинсталляция модуля 'Логирование инфоблоков'", $DOCUMENT_ROOT."/local/modules/iblocklog/install/unstep.php");
        
		return true;
    }
}