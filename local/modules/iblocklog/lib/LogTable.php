<?php
namespace Iblocklog\D7;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM;
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class LogTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> DATE_UPDATE datetime optional
 * <li> MOD_USER_ID int optional
 * <li> USER_NAME reference to {@link \Bitrix\User\UserTable}
 * <li> ELEMENT_ID int optional
 * </ul>
 *
 * @package Bitrix\Iblock
 **/

class LogTable extends Entity\DataManager
{
    const TYPE_TEXT = 'text';
    const TYPE_HTML = 'html';

    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'c_iblock_element_log';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => new ORM\Fields\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('IBLOCKLOG_ID_FIELD'),
            )),
            'DATE_UPDATE' => new ORM\Fields\DatetimeField('DATE_UPDATE', array(
                'default_value' => function()
                {
                    return new Main\Type\DateTime();
                },
                'title' => Loc::getMessage('IBLOCKLOG_DATE_UPDATE'),
            )),
            'MOD_USER_ID' => new ORM\Fields\IntegerField('MOD_USER_ID', array(
                'title' => Loc::getMessage('IBLOCKLOG_MOD_USER_ID'),
            )),
            'USER_NAME' => new ORM\Fields\StringField('USER_NAME', array(
                'validation' => array(__CLASS__, 'validateName'),
                'title' => Loc::getMessage('IBLOCKLOG_ENTITY_NAME_FIELD'),
            )),
            'ELEMENT_ID' => new ORM\Fields\IntegerField('ELEMENT_ID', array(
                'title' => Loc::getMessage('IBLOCKLOG_ELEMENT_ID'),
            )),
            'IBLOCK_ID' => new ORM\Fields\IntegerField('IBLOCK_ID', array(
                'title' => Loc::getMessage('IBLOCKLOG_IBLOCK_ID'),
            )),
        );
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return array(
            new ORM\Fields\Validators\LengthValidator(null, 100),
        );
    }
}