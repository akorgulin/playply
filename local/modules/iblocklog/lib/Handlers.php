<?php


namespace Iblocklog\D7;

use Bitrix\Main\Type;
use Iblocklog\D7\LogTable;
use Bitrix\Main\Localization\Loc;

class Handlers
{
    public function handlerIblockElementAdd(&$arFields){
        if($arFields["ID"]) {
            LogTable::add(
                [
                    "DATE_UPDATE" => new Type\Date(date("Y").'-'.date("m").'-'.date("d"), 'Y-m-d'),
                    "MOD_USER_ID" => $GLOBALS["USER"]->GetID(),
                    "USER_NAME" => $GLOBALS["USER"]->GetLogin()??$GLOBALS["USER"]->GetFullName(),
                    'ELEMENT_ID' => $arFields["ID"],
                    'IBLOCK_ID' => $arFields["IBLOCK_ID"],
                ]
            );
        }
    }
}