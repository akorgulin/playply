<?php
IncludeModuleLangFile(__FILE__);
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */

if ($APPLICATION->GetGroupRight("iblocklog") != "D")
{
	$aMenu = array(
		"parent_menu" => "global_menu_settings",
		"section" => "iblocklog",
		"sort" => 1850,
		"text" => GetMessage("IBLOCKLOG_MNU_SECT"),
		"title" => GetMessage("IBLOCKLOG_MNU_SECT_TITLE"),
		"icon" => "iblocklog_menu_icon",
		"page_icon" => "iblocklog_page_icon",
		"items_id" => "menu_iblocklog",
		"items" => array(
		),
	);

	$aMenu["items"][] = array(
		"text" => GetMessage("IBLOCKLOG_QUEUE"),
		"url" => "/bitrix/admin/perfmon_table.php?lang=ru&table_name=b_iblocklog",
		"more_url" => array(),
		"title" => GetMessage("IBLOCKLOG_QUEUE_ALT"),
	);

	return $aMenu;
}
return false;
