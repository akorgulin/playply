<?php
$MESS["MAIN_TAB_UNQUOTABLE"] = "Фильтр нецензурных слов";
$MESS["MAIN_TAB_TITLE_UNQUOTABLE"] = "Фильтр нецензурных слов";
$MESS["FILTER"] = 'Включить фильтр нецензурных слов:';
$MESS["FILTER_ACTION"] = "Действия с найденными словами";
$MESS["USE_FILTER"] = "Фильтр нецензурных слов";
$MESS["FILTER_RPL"] = "Замена";
$MESS["non"] = " Ничего ";
$MESS["rpl"] = " Заменить ";
$MESS["del"] = " Удалить ";
$MESS["ASSOC_LANG_PARAMS"] = "Языкозависимые параметры:";
$MESS["LANG"] = "Язык";
$MESS["DICTINARY_AND_EREG"] = "Словарь слов и<br> регулярных выражений";
$MESS["TRANSCRIPTION_DICTIONARY"] = "Словарь транслита";
$MESS["DICTIONARY_NONE"] = "Не установлен";
$MESS["MAIN_SAVE"] = "Сохранить";
$MESS["MAIN_RESET"] = "Сбросить";
$MESS["MAIN_OPT_SAVE_TITLE"] = "Сохранить изменения и вернуться";
$MESS["MAIN_OPT_APPLY"] = "Применить";
$MESS["MAIN_OPT_APPLY_TITLE"] = "Сохранить изменения и остаться в форме";
$MESS["MAIN_OPT_CANCEL"] = "Отменить";
$MESS["MAIN_OPT_CANCEL_TITLE"] = "Не сохранять изменения и вернуться";
$MESS["MAIN_RESTORE_DEFAULTS"] = "По умолчанию";
?>