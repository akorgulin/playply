<?php
defined('B_PROLOG_INCLUDED') || die;

/** @var CBitrixComponent $this */

use Bitrix\Main\Loader;
use Intervolga\Sed\ComponentTemplateLevel\ComponentTemplate;


if (!Loader::includeModule('intervolga.sed')) {
    return;
}

ComponentTemplate::extendEpilogue($this);
