<?php
defined('B_PROLOG_INCLUDED') || die;

/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Loader;
use Intervolga\Sed\ComponentTemplateLevel\ComponentTemplate;


if (!Loader::includeModule('intervolga.sed')) {
    ShowError('Error: module intervolga.sed not installed.');
    return;
}

ComponentTemplate::extendMutator($this->getComponent());
