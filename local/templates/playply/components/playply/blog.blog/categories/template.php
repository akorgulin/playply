<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!$this->__component->__parent || empty($this->__component->__parent->__name) || $this->__component->__parent->__name != "bitrix:blog"):
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/style.css');
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/themes/blue/style.css');
endif;

?>
<? CUtil::InitJSCore(array("image")); ?>

<div class="cat_list_open" data-aos="flip-up" data-aos-delay="500"><span class="lnr lnr-chevron-down"></span>Показать
    категории
</div>
<?
$arGroups = [];
foreach ($arResult["POST"] as $ind => $CurPost) {

    foreach ($CurPost["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField){

        if($arPostField["FIELD_NAME"] == "UF_BLOG_POST_GROUP") {
            if (!empty($arPostField["VALUE"])) {
                $arGroups[$arPostField["VALUE"]] = $arPostField["VALUE"];
            }
        }
        ?>
    <?}
}

$arResult['CATEGORY_PROPERTY'] = [];
Bitrix\Main\Loader::includeModule("highloadblock");

$dbHblock = Bitrix\Highloadblock\HighloadBlockTable::getList(
    array(
        "filter" => array("NAME" => "CategoryReference", "TABLE_NAME" => "eshop_category_reference")
    )
);
if ($hldata = $dbHblock->Fetch())
{
    $ID = $hldata["ID"];
    $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById($ID)->fetch();
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $entity_data_class = $hlentity->getDataClass();
    $entity_table_name = $hldata['TABLE_NAME'];
    $sTableID = 'tbl_'.$entity_table_name;

    $rsData = $entity_data_class::getList();

    while ($arEnum = $rsData->fetch())
    {

        $boolPict = true;
        if (!isset($arEnum['UF_NAME']))
        {
            $boolName = false;
            break;
        }

        if (!isset($arEnum['UF_FILE']) || (int)$arEnum['UF_FILE'] <= 0)
            $boolPict = false;

        if ($boolPict)
        {
            $arEnum['PREVIEW_PICTURE'] = CFile::GetFileArray($arEnum['UF_FILE']);
            if (empty($arEnum['PREVIEW_PICTURE']))
                $boolPict = false;
        }

        $descrExists = (isset($arEnum['UF_DESCRIPTION']) && (string)$arEnum['UF_DESCRIPTION'] !== '');
        if ($boolPict)
        {
            if ($descrExists)
            {
                $width = $arParams["WIDTH_SMALL"];
                $height = $arParams["HEIGHT_SMALL"];
                $type = "PIC_TEXT";
            }
            else
            {
                $width = $arParams["WIDTH"];
                $height = $arParams["HEIGHT"];
                $type = "ONLY_PIC";
            }

            $arEnum['PREVIEW_PICTURE']['WIDTH'] = (int)$arEnum['PREVIEW_PICTURE']['WIDTH'];
            $arEnum['PREVIEW_PICTURE']['HEIGHT'] = (int)$arEnum['PREVIEW_PICTURE']['HEIGHT'];
            if (
                $arEnum['PREVIEW_PICTURE']['WIDTH'] > $width
                || $arEnum['PREVIEW_PICTURE']['HEIGHT'] > $height
            )
            {
                $arEnum['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
                    $arEnum['PREVIEW_PICTURE'],
                    array("width" => $width, "height" => $height),
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true
                );

                $arEnum['PREVIEW_PICTURE']['SRC'] = $arEnum['PREVIEW_PICTURE']['src'];
                $arEnum['PREVIEW_PICTURE']['WIDTH'] = $arEnum['PREVIEW_PICTURE']['width'];
                $arEnum['PREVIEW_PICTURE']['HEIGHT'] = $arEnum['PREVIEW_PICTURE']['height'];
            }
        }

        $arResult['CATEGORY_PROPERTY'][$arEnum["ID"]] = $arEnum;
    }

}

?>
<ul class="categories_list inversion clearfix aos-init aos-animate" data-aos="flip-up" data-aos-delay="500">
    <?foreach ($arResult['CATEGORY_PROPERTY'] as $ind => $group) {?>
        <li <?if($_REQUEST["group"]==$group["UF_CODE"]){?>class="current"<?}?>><a href="/blog/group/<?=$group["UF_CODE"]?>/"><?=$group["UF_NAME"]?></a></li>
    <?}?>
</ul>