<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!$this->__component->__parent || empty($this->__component->__parent->__name) || $this->__component->__parent->__name != "bitrix:blog"):
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/style.css');
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/blog/templates/.default/themes/blue/style.css');
endif;

?>
<? CUtil::InitJSCore(array("image")); ?>

<?if (count($arResult["POST"]) > 0) { ?>
<!--Category-->
<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog_head clearfix">
                    <div class="title" data-aos="flip-up" data-aos-delay="200"><?=$arResult["BLOG"]["NAME"]?></div>
                    <div class="morelink" data-aos="fade-left" data-aos-delay="400"> <a href="/blog/" class="btn white iconlink">все записи из блога <span class="lnr lnr-arrow-right"></span></a> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?
            if (!empty($arResult["OK_MESSAGE"])) {
                ?>
                <div class="blog-notes blog-note-box">
                    <div class="blog-note-text">
                        <ul>
                            <?
                            foreach ($arResult["OK_MESSAGE"] as $v) {
                                ?>
                                <li><?= $v ?></li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?
            }
            if (!empty($arResult["MESSAGE"])) {
                ?>
                <div class="blog-textinfo blog-note-box">
                    <div class="blog-textinfo-text">
                        <ul>
                            <?
                            foreach ($arResult["MESSAGE"] as $v) {
                                ?>
                                <li><?= $v ?></li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?
            }
            if (!empty($arResult["ERROR_MESSAGE"])) {
                ?>
                <div class="blog-errors blog-note-box blog-note-error">
                    <div class="blog-error-text">
                        <ul>
                            <?
                            foreach ($arResult["ERROR_MESSAGE"] as $v) {
                                ?>
                                <li><?= $v ?></li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?
            }

            if (count($arResult["POST"]) > 0) {
                $i=0;
                foreach ($arResult["POST"] as $ind => $CurPost) {
                    if($i>=3) break;

                    $className = "blog-post";
                    if ($ind == 0)
                        $className .= " blog-post-first";
                    elseif (($ind + 1) == count($arResult["POST"]))
                        $className .= " blog-post-last";
                    if ($ind % 2 == 0)
                        $className .= " blog-post-alt";
                    $className .= " blog-post-year-" . $CurPost["DATE_PUBLISH_Y"];
                    $className .= " blog-post-month-" . IntVal($CurPost["DATE_PUBLISH_M"]);
                    $className .= " blog-post-day-" . IntVal($CurPost["DATE_PUBLISH_D"]);

                    $horisontal = false;
                    $class= false;
                    foreach ($CurPost["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField){?>
                        <?
                        if($arPostField["FIELD_NAME"]=="UF_BLOG_POST_CAT" && !empty($arPostField["VALUE"]) && $arPostField["VALUE"]==329) {
                            $horisontal = false;
                            $class = "without_overlay without_overlay aos-init aos-animate";
                        } else if($arPostField["FIELD_NAME"]=="UF_BLOG_POST_CAT" && !empty($arPostField["VALUE"]) && $arPostField["VALUE"]==330) {
                            $horisontal = true;
                            $class= "";
                        }
                        ?>
                    <?}

                    foreach ($CurPost["POST_PROPERTIES"]["DATA"] as $FIELD_NAME => $arPostField) {?>
                        <?
                        if($arPostField["FIELD_NAME"] == "UF_BLOG_POST_PIC" && $horisontal) {
                            if (!empty($arPostField["VALUE"]) && $arPostField["FIELD_NAME"] == "UF_BLOG_POST_PIC") {

                                $itemDate = CIBlockFormatProperties::DateFormat('d \<\s\p\a\n\>F\<\/\s\p\a\n\> Y', strtotime($CurPost["DATE_PUBLISH_DATE"]));

                                $arFile = CFile::GetFileArray($arPostField["VALUE"]);
                                $html = "";
                                $html = '<div class="image" data-aos="fade-left" data-aos-delay="300">';
                                $html .= '<a href="'.$CurPost["urlToPost"].'"><img src="'.$arFile["SRC"].'" alt="" class="js-tilt"></a>';
                                $html .= '</div>';
                                $html .= '<div class="date">'.$itemDate.'</div>';
                                //$html .= '<div class="post_title" data-aos="fade-left" data-aos-delay="400"><a href="/blog/detail.php?page=blog&blog=fijiblognew&id='.$CurPost["ID"].'" title="'. $CurPost["TITLE"] .'">'. $CurPost["TITLE"] .'</a></div>';
                                $html .= '<div class="description" data-aos="fade-left" data-aos-delay="500">'.$CurPost["TEXT_FORMATED"].'</div>';

                            }
                        }
                        ?>
                    <?} ?>
                    <?if($html!="") { ?>
                        <div class="col-4">
                            <div class="post_item" data-aos="fade-left" data-aos-delay="600">
                                <?= $html ?>
                            </div>
                        </div>
                        <?
                    }
                    $i++;
                }
                //if (strlen($arResult["NAV_STRING"]) > 0)
                //    echo $arResult["NAV_STRING"];
            } elseif (!empty($arResult["BLOG"]))
                echo GetMessage("BLOG_BLOG_BLOG_NO_AVAIBLE_MES");
            ?>
        </div>
    </div>
</section>
<? } ?>