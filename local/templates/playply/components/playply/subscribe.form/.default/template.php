<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>
<section class="subscribe_block">
    <div class="wrapper center">
        <div class="title" data-aos="flip-up">Подписаться на рассылку</div>
        <div class="description" data-aos="fade-up" data-aos-delay="400">Будь в курсе новинок и выгодных
            предложений института красоты Fijie!</div>
        <div class="form" data-aos="fade-up" data-aos-delay="200">
        <?
        $frame = $this->createFrame("subscribe-form", false)->begin();
        ?>
            <form id="subscribeform" action="<?=$arResult["FORM_ACTION"]?>">
                <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
                    <label class="hidden" for="sf_RUB_ID_<?=$itemValue["ID"]?>">
                        <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /> <?=$itemValue["NAME"]?>
                    </label><br />
                <?endforeach;?>
                <input type="text" name="sf_EMAIL" size="20"placeholder="<?=GetMessage("subscr_form_email_title")?>"  value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>" />
                <a data-fancybox="" data-src="#subscribe" href="javascript:;"><input type="submit" class="btn subscribe_up" name="OK" value="<?=GetMessage("subscr_form_button")?>" /></a>
            </form>
        <?
        $frame->beginStub();
        ?>
            <form id="subscribeform" action="<?=$arResult["FORM_ACTION"]?>">
                <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
                    <label class="hidden" for="sf_RUB_ID_<?=$itemValue["ID"]?>">
                        <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>" /> <?=$itemValue["NAME"]?>
                    </label><br />
                <?endforeach;?>

                <input type="text" name="sf_EMAIL" size="20" value="" placeholder="<?=GetMessage("subscr_form_email_title")?>"  title="<?=GetMessage("subscr_form_email_title")?>" />
                <a data-fancybox="" data-src="#subscribe" href="javascript:;"><input type="submit" class="btn subscribe_up" name="OK" value="<?=GetMessage("subscr_form_button")?>" /></a>
            </form>
        <?
        $frame->end();
        ?>
        </div>
    </div>
    <div class="left_image" data-aos="fade-down" data-aos-delay="400"></div>
    <div class="right_image" data-aos="fade-up" data-aos-delay="600"></div>
</section>
