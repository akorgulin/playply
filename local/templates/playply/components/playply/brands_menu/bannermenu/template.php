<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul>
    <? foreach($arResult["BRANDS"] as $itemBrandRow) {  ?>
        <li><a <?if($itemBrandRow["CODE"]) { ?> href="/brands/<?=strtolower($itemBrandRow["CODE"])?>/" <?}?>><?=$itemBrandRow["NAME"]?></a></li>
    <? } ?>
</ul>