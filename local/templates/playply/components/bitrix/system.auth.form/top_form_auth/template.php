<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="str_up"></div>
<div class="popup_name">Вход в<br>личный кабинет</div>

<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>

<?if($arResult["FORM_TYPE"] == "login"):?>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
	<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />

    <div class="for_input shadow_input cr13">
			<input type="text" name="USER_LOGIN" placeholder="<?=GetMessage("AUTH_LOGIN_NEW")?>" autocomplete="off" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="17" /></td>
	</div>
    <div class="for_input shadow_input cr13">
			<input type="password" name="USER_PASSWORD" placeholder="<?=GetMessage("AUTH_PASSWORD_NEW")?>" autocomplete="off" maxlength="50" size="17" />
    </div>
    <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE_NEW")?>" style="display:none">
                        <div class="bx-auth-secure-icon"></div>
                    </span>
                    <noscript>
                    <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE_NEW")?>">
                        <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                    </span>
                    </noscript>
                    <script type="text/javascript">
                    document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
                    </script>
    <?endif?>

<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
		<table style="display: none;"><tr>
			<td valign="top"><input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME_NEW")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT_NEW")?></label></td>
		</tr></table>
<?endif?>
<?if ($arResult["CAPTCHA_CODE"]):?>
        <table>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT_NEW")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr></table>
<?endif?>
		<input type="submit" name="Login" class="red_submit shadow" value="<?=GetMessage("AUTH_LOGIN_BUTTON_NEW")?>" />

    <p><noindex><a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" class="gray forgot_passwd" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2_NEW")?></a></noindex></p>
<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
        <p><noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>"  id="link_registration" rel="nofollow"><?=GetMessage("AUTH_REGISTER_NEW")?></a></noindex></p>
<?endif?>

<?if($arResult["AUTH_SERVICES"]==23434):?>
        <table>
		<tr>
			<td colspan="2">
				<div class="bx-auth-lbl"><?=GetMessage("socserv_as_user_form_new")?></div>
<?
$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);
?>
			</td>
		</tr></table>
<?endif?>

</form>

<?if($arResult["AUTH_SERVICES"]):?>
<?
/*$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
);*/
?>
<?endif?>

<?
elseif($arResult["FORM_TYPE"] == "otp"):
?>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?if($arResult["BACKURL"] <> ''):?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="OTP" />
    <div class="for_input shadow_input cr13">
			<input type="text" placeholder="<?echo GetMessage("auth_form_comp_otp_new")?>" autocomplete="off" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" />
    </div>
<?if ($arResult["CAPTCHA_CODE"]):?>
    <table>
		<tr>
			<td colspan="2">
			<?echo GetMessage("AUTH_CAPTCHA_PROMT_NEW")?>:<br />
			<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
			<input type="text" name="captcha_word" maxlength="50" value="" /></td>
		</tr> </table>
<?endif?>
<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
    <table>
		<tr>
			<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
			<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title_new")?>"><?echo GetMessage("auth_form_comp_otp_remember_new")?></label></td>
		</tr> </table>
<?endif?>
		<input type="submit" name="Login" class="red_submit shadow" value="<?=GetMessage("AUTH_LOGIN_BUTTON_NEW")?>" />
        <p><noindex><a id="link_registration" href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth_new")?></a></noindex></p>
</form>

<?
else:
?>

<form action="<?=$arResult["AUTH_URL"]?>">
			<?=$arResult["USER_NAME"]?>&nbsp;[<?=$arResult["USER_LOGIN"]?>]<br />
			<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE_NEW")?>"><?=GetMessage("AUTH_PROFILE_NEW")?></a><br />
			<?foreach ($arResult["GET"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="logout" value="yes" />
			<input type="submit" class="red_submit shadow" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON_NEW")?>" />
</form>
<?endif?>

