<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<!--Authorisation page-->
<section class="content_wrapper registration">
    <div class="parallax_01" data-aos="fade-down" data-aos-delay="350"></div>
    <div class="parallax_02" data-aos="fade-up" data-aos-delay="450"></div>
    <div class="parallax_03" data-aos="fade-down" data-aos-delay="450"></div>
    <div class="parallax_04" data-aos="fade-right" data-aos-delay="450"></div>
    <div class="container page_registration_wrapper">
        <div class="row">
            <div class="col-12 center">
                <?if($arResult["AUTH_SERVICES"]):?>
                    <h1 class="page_heading" data-aos="flip-up" data-aos-delay="200"><?echo GetMessage("AUTH_TITLE")?></h1>
                <?endif?>
                <form name="form_auth" method="post" target="_top" action="<?=SITE_DIR?>auth/<?//=$arResult["AUTH_URL"]?>" class="bx_auth_form">
                    <input type="hidden" name="AUTH_FORM" value="Y" />
                    <input type="hidden" name="TYPE" value="AUTH" />
                    <?if (strlen($arParams["BACKURL"]) > 0 || strlen($arResult["BACKURL"]) > 0):?>
                        <input type="hidden" name="backurl" value="<?=($arParams["BACKURL"] ? $arParams["BACKURL"] : $arResult["BACKURL"])?>" />
                    <?endif?>
                    <?foreach ($arResult["POST"] as $key => $value):?>
                        <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                    <?endforeach?>

                    <p class="description" data-aos="fade-up" data-aos-delay="300">Зарегистрируйтесь на нашем сайте и Вам будут доступны уникальные цены и открыты дополнительные разделы сайта.</p>

                    <?
                    ShowMessage($arParams["~AUTH_RESULT"]);
                    ShowMessage($arResult['ERROR_MESSAGE']);
                    ?>
                    <div class="forms-grid">
                        <div class="forms-col-6 left" data-aos="fade-right" data-aos-delay="350">
                            <div class="border-form">
                                <div class="without_icon_input">
                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                    <input type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN_NEW"]?>" placeholder="<?=GetMessage("AUTH_LOGIN_NEW")?>">
                                </div>
                            </div>
                        </div>
                        <div class="forms-col-6 right" data-aos="fade-left" data-aos-delay="400">
                            <div class="border-form">
                                <div class="without_icon_input">
                                    <div class="icon"><span class="lnr lnr-user"></span></div>
                                    <input type="password" name="USER_PASSWORD" maxlength="255" placeholder="<?=GetMessage("AUTH_PASSWORD_NEW")?>">
                                </div>
                            </div>
                        </div>

                        <div class="forms-col-6 right" data-aos="fade-left" data-aos-delay="400">
                            <div class="border-form">
                                <div class="without_icon_input">
                                    <?if($arResult["SECURE_AUTH"]):?>
                                        <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE_NEW")?>" style="display:none">
                                                <div class="bx-auth-secure-icon"></div>
                                        </span>
                                        <noscript>
                                            <span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE_NEW")?>">
                                                <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                                            </span>
                                        </noscript>
                                        <script type="text/javascript">
                                            document.getElementById('bx_auth_secure').style.display = 'inline-block';
                                        </script>
                                    <?endif?>
                                </div>
                            </div>
                        </div>

                        <div class="forms-col-6 right" data-aos="fade-left" data-aos-delay="400">
                            <div class="border-form">
                                <div class="without_icon_input">
                                    <?if($arResult["CAPTCHA_CODE"]):?>
                                        <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                                        <?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:
                                        <input class="bx-auth-input" type="text" name="captcha_word" maxlength="50" value="" size="15" />
                                    <?endif;?>
                                </div>
                            </div>
                        </div>

                        <?if ($arResult["STORE_PASSWORD"] == "Y"):?>
                            <span class="rememberme"><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" checked/>&nbsp;<?=GetMessage("AUTH_REMEMBER_ME_NEW")?></span>
                        <?endif?>
                    </div>

                    <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                        <noindex>
                            <span class="forgotpassword"><a href="<?=$arParams["AUTH_FORGOT_PASSWORD_URL"] ? $arParams["AUTH_FORGOT_PASSWORD_URL"] : $arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2_NEW")?></a></span>
                        </noindex>
                    <?endif?>

                    <div class="buttons_link">
                        <input type="submit" name="Login" class="bt_blue big shadow" value="<?=GetMessage("AUTH_AUTHORIZE_NEW")?>" style="display:none;"/>
                        <a href="#Login" onclick="javascript:$('form[name=form_auth] input[type=submit]').click();" class="btn mint iconlink" data-aos="fade-up" data-aos-delay="500"><?=GetMessage("AUTH_AUTHORIZE_NEW")?><span class="lnr lnr-arrow-right"></span></a>
                    </div>
                    <p class="info_small" data-aos="fade-up" data-aos-delay="550">Нажимая на кнопку «Зарегистрироваться», я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и <a href="#">политикой конфиденциальности</a>.</p>
                </form>
                <script type="text/javascript">
                    <?if (strlen($arResult["LAST_LOGIN"])>0):?>
                    try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
                    <?else:?>
                    try{document.form_auth.USER_LOGIN.focus();}catch(e){}
                    <?endif?>
                </script>
            </div>
        </div>
    </div>



    <?$APPLICATION->IncludeFile(
        SITE_TEMPLATE_PATH."/include/advantages.php",
        Array(),
        Array("MODE"=>"text")
    );?>

</section>

