<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="item" data-aos="fade-left" data-aos-delay="250">
    <div class="border">
        <div class="discount">
            <h6 class="area_title">Текущая скидка: -5%</h6>
            <p><strong>До скидки -10% осталось купить товаров на сумму 7 850 руб.</strong></p>
            <div class="discount_lines">
                <div class="line">
                    <div class="scroll_area"></div>
                    <div class="descr">От 20 000 руб. – 5% скидка</div>
                </div>
                <div class="line">
                    <div class="scroll_area x2"></div>
                    <div class="descr">От 30 000 руб. – 10% скидка</div>
                </div>
                <div class="line">
                    <div class="scroll_area x3"></div>
                    <div class="descr">От 40 000 руб. – 15% скидка</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?
if (empty($arResult))
{
	?><h3><? echo GetMessage('BX_CMP_CDI_TPL_MESS_NO_DISCOUNT_SAVE'); ?></h3><?
}
else
{
	?><h3><? echo GetMessage('BX_CMP_CDI_TPL_MESS_DISCOUNT_SAVE'); ?></h3><?
	foreach ($arResult as &$arDiscountSave)
	{
		?><h4><? echo $arDiscountSave['NAME']; ?></h4><?
		?><p><? echo GetMessage('BX_CMP_CDI_TPL_MESS_SIZE'); ?> <?
		if ('P' == $arDiscountSave['VALUE_TYPE'])
		{
			echo $arDiscountSave['VALUE']; ?>&nbsp;%<?
		}
		else
		{
			echo CCurrencyLang::CurrencyFormat($arDiscountSave['VALUE'], $arDiscountSave['CURRENCY'], true);
		}
		if (isset($arDiscountSave['NEXT_LEVEL']) && is_array($arDiscountSave['NEXT_LEVEL']))
		{
			$strNextLevel = '';
			if ('P' == $arDiscountSave['NEXT_LEVEL']['VALUE_TYPE'])
			{
				$strNextLevel = $arDiscountSave['NEXT_LEVEL']['VALUE'].'&nbsp;%';
			}
			else
			{
				$strNextLevel = CCurrencyLang::CurrencyFormat($arDiscountSave['NEXT_LEVEL']['VALUE'], $arDiscountSave['CURRENCY'], true);
			}

			?><br /><? echo str_replace(array('#SIZE#', '#SUMM#'), array($strNextLevel, CCurrencyLang::CurrencyFormat(($arDiscountSave['NEXT_LEVEL']['RANGE_FROM'] - $arDiscountSave['RANGE_SUMM']),$arDiscountSave['CURRENCY'], true)), GetMessage('BX_CMP_CDI_TPL_MESS_NEXT_LEVEL')); ?><?
		}
		?></p><br /><?
	}
}
?>