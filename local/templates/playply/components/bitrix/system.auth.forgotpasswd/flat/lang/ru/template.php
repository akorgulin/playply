<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "Если вы забыли пароль, введите логин или E-Mail. Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.";
$MESS ['AUTH_GET_CHECK_STRING'] = "Восстановление пароля";
$MESS ['AUTH_SEND'] = "Отправить";
$MESS ['AUTH_AUTH'] = "Авторизация";
$MESS["AUTH_LOGIN_EMAIL"] = "Введите свой e-mail";
$MESS["system_auth_captcha"] = "Введите символы с картинки";
?>