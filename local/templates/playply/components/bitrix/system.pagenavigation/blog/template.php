<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

$colorSchemes = array(
    "green" => "bx-green",
    "yellow" => "bx-yellow",
    "red" => "bx-red",
    "blue" => "bx-blue",
);
if (isset($colorSchemes[$arParams["TEMPLATE_THEME"]])) {
    $colorScheme = $colorSchemes[$arParams["TEMPLATE_THEME"]];
} else {
    $colorScheme = "";
}
if($arResult["NavPageNomer"]==$arResult["NavPageCount"]) {
    $arResult["NavPageNomer"] = 1;
}

?>
<? if (($arResult["NavPageNomer"]*$arResult["NavPageCount"])<$arResult["NavRecordCount"]) { ?>

    <div id="btn_" class="center btn_more" data-aos="fade-up" data-aos-delay="400">
        <a data-ajax-id="" href="javascript:void(0)" data-show-more="<?= $arResult["NavNum"] ?>"
           data-next-page="<?= ($arResult["NavPageNomer"]+1) ?>"
           data-max-page="<?= $arResult["nStartPage"] ?>" <? $arResult["sUrlPath"] ?><? $strNavQueryString ?><? $arResult["NavNum"] ?><? ($arResult["NavPageNomer"]+1 ) ?>
           class="ajax_loadmore"><span class="lnr lnr-arrow-down"></span>Загрузить ещё</a>
    </div>
    <script type="text/javascript">

        $("div.btn_more").attr("id",bxAjaxId);
        $("div.btn_more>a").attr("data-ajax-id",bxAjaxId);
        $("div.btn_more>a").data("ajax-id",bxAjaxId);

        $(document).on('click', '[data-show-more]', function () {
            var btn = $(this);
            var page = btn.attr('data-next-page');
            var id = btn.attr('data-show-more');
            var bx_ajax_id = btn.attr('data-ajax-id');
            var block_id = "#comp_" + bx_ajax_id;

            var data = {
                bxajaxid: bx_ajax_id
            };
            data['PAGEN_' + id] = page;

            $.ajax({
                type: "GET",
                url: window.location.href,
                data: data,
                timeout: 3000,
                dataType: "html",
                success: function (data) {
                    $("div.btn_more").remove();
                    $blockAppend = $(data).find(block_id).find("div.item");
                    $more = $(data).find("div.btn_more");
                    $(block_id).find("div.item").last().after($blockAppend);
                    $(block_id).find("div.item").last().after($more);
                    $("div.btn_more").attr("id",bxAjaxId);
                    $("div.btn_more>a").attr("data-ajax-id",bxAjaxId);
                    $("div.btn_more>a").data("ajax-id",bxAjaxId);

                    $("section.content_wrapper").imagesLoaded(function() {
                        $("section.content_wrapper").find(block_id).masonry( 'appended', $blockAppend, true );
                    });
                }
            });
        });
    </script>
<? } ?>

