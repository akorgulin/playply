<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;

/*
 * <div class="col-2 col-lg-1 pl-0 d-none d-md-block">
    <nav class="footer__navigation">
        <span class="navigation-title">Мебель</span>
        <a class="navigation_link navigation_link--category" href="#">Серия CLIC</a>
        <a class="navigation_link navigation_link--category" href="#" data-teleport-target="footer__nav-first"> </a>
        <a class="navigation_link navigation_link--category" href="#" data-teleport-target="footer__nav-second"> </a>
        <a class="navigation_link navigation_link--category" href="#" data-teleport-target="footer__nav-third"> </a>
        <a class="navigation_link" href="#">Столы</a>
        <a class="navigation_link" href="#">Стулья</a>
        <a class="navigation_link" href="#">Этажерка</a>
        <a class="navigation_link" href="#">Стул для кормления</a>
    </nav>
</div>
<div class="col-lg-1 d-none d-lg-block">
    <nav class="footer__navigation">
        <span class="navigation-title"></span>
        <a class="navigation_link navigation_link--category" href="#" js-teleport data-teleport-to="footer__nav-first" data-teleport-condition="&lt;992">Серия CLIC</a>
        <a class="navigation_link" href="#">Столы</a><a class="navigation_link" href="#">Стулья</a>
        <a class="navigation_link" href="#">Этажерка</a><a class="navigation_link" href="#">Стул для
            кормления</a>
    </nav>
</div>
<div class="col-lg-1 d-none d-lg-block">
    <nav class="footer__navigation"><span class="navigation-title"></span><a
                class="navigation_link navigation_link--category" href="#" js-teleport
                data-teleport-to="footer__nav-second" data-teleport-condition="&lt;992"> Серия CLIC</a><a
                class="navigation_link" href="#">Столы</a><a class="navigation_link" href="#">Стулья</a><a
                class="navigation_link" href="#">Этажерка</a><a class="navigation_link" href="#">Стул для
            кормления</a></nav>
</div>
<div class="col-lg-1 d-none d-lg-block">
    <nav class="footer__navigation">
        <span class="navigation-title"></span>
        <a class="navigation_link navigation_link--category" href="#" js-teleport data-teleport-to="footer__nav-third" data-teleport-condition="&lt;992"> Серия CLIC</a>
        <a class="navigation_link" href="#">Столы</a><a class="navigation_link" href="#">Стулья</a>
        <a class="navigation_link" href="#">Этажерка</a><a class="navigation_link" href="#">Стул для кормления</a>
    </nav>
</div>
<div class="col-3 col-lg-1 pr-0 d-none d-md-block">
    <nav class="footer__navigation">
        <span class="navigation-title">Аксессуары</span>
        <a class="navigation_link" href="#">Столы</a>
        <a class="navigation_link" href="#">Стулья</a>
        <a class="navigation_link" href="#">Этажерка</a>
        <a class="navigation_link" href="#">Стул для кормления</a>
    </nav>
</div>
 *
 * */
?>



<? $i=0; ?>
<?foreach($arResult["ALL_ITEMS_ID"] as $itemIdLevel_1=>$arItemsLevel_2):?> <!-- first level-->
    <div class="<?if($i==0 || count($arResult["ALL_ITEMS_ID"])==$i) {?>col-2 col-lg-1 pl-0 d-none d-md-block<?} else {?>col-lg-1 d-none d-lg-block<?}?>">
        <nav class="footer__navigation">
            <span class="navigation-title"><?=$arResult["ALL_ITEMS"][$itemIdLevel_1]["TEXT"]?></span>
            <?if (is_array($arItemsLevel_2) && !empty($arItemsLevel_2)):?>
                <?foreach($arItemsLevel_2 as $itemIdLevel_2=>$arItemsLevel_3):?>
                    <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>" class="navigation_link navigation_link--category"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
                    <?if(is_array($arItemsLevel_3)) { ?>
                        <a class="navigation_link navigation_link--category" href="#" data-teleport-target="footer__nav-first"> </a>
                        <a class="navigation_link navigation_link--category" href="#" data-teleport-target="footer__nav-second"> </a>
                        <a class="navigation_link navigation_link--category" href="#" data-teleport-target="footer__nav-third"> </a>
                    <? }?>
                    <?if (is_array($arItemsLevel_3) && !empty($arItemsLevel_3)):?>
                        <?foreach($arItemsLevel_3 as $itemIdLevel_3):?>
                            <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>" class="navigation_link"><?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?></a>
                        <?endforeach?>
                    <?endif?>
                <?endforeach?>
            <?endif?>
        </nav>
    </div>
    <? $i++; ?>
<?endforeach?>

