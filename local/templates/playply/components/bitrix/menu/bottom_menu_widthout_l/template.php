<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;

?>

<ul class="footer-new__menu-item">
	<?foreach($arResult as $itemIdex => $arItem):?>
		<li class="<?if ($arItem["SELECTED"]):?>active <?endif;?><?if(!empty($arItem['PARAMS']['class'])):?> <?=$arItem['PARAMS']['class']?><?endif;?>">
			<a href="<?=$arItem["LINK"]?>" title="<?=$arItem["TEXT"];?>"><?=$arItem["TEXT"];?></a>
		</li>
	<?endforeach;?>
</ul>