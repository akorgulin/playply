<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CUtil::InitJSCore();
CJSCore::Init(array("fx", "jquery"));
//$curPage = $APPLICATION->GetCurPage(true);
CAjax::Init();
/* Редирект в корзину для ошибочных ссылок покупки без которых товары Ajax-ом в корзину не кладутся*/
if ($APPLICATION->getCurDir() == "/basket/personal/cart/" || $APPLICATION->getCurDir() == "/basket/personal/") {
    LocalRedirect("/personal/cart/", 302);
}
if ($_REQUEST["logout"] == "yes") {
    LocalRedirect("/", 301);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="Content-Language" content="ru">

    <title><?php $APPLICATION->ShowTitle() ?></title>
    <?php
    echo '<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '"' . (true ? ' /' : '') . '>' . "\n";
    $APPLICATION->ShowMeta("robots", false, true);
    $APPLICATION->ShowMeta("keywords", false, true);
    $APPLICATION->ShowMeta("description", false, true);
    $APPLICATION->ShowCSS(true, true);
    ?>
    <?php
    $APPLICATION->ShowHeadStrings();
    $APPLICATION->ShowHeadScripts();
    ?>
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/assets/css/app.css">
</head>
<body>
<header>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <div class="container">
        <div class="row">
            <div class="col-auto col-md-2 mr-sm-auto">
                <a class="header__logo" href="#">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/logo.php",
                        Array(),
                        Array("MODE"=>"text")
                    );?>
                </a>
            </div>
            <div class="col-auto order-last order-md-2 mr-auto">
                <div class="header__burger d-md-none" js-menu>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#fff">
                        <rect x="2" y="5" width="20" height="2" rx="1"/>
                        <rect x="2" y="11" width="20" height="2" rx="1"/>
                        <rect x="2" y="17" width="14" height="2" rx="1"/>
                    </svg>
                </div>
                <nav class="header__navigation">
                    <div class="navigation__mobile d-md-none">
                        <img class="img-responsive" src="<?= SITE_TEMPLATE_PATH ?>/assets/images/svg/logo.svg" alt="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             js-menu>
                            <path d="M6.41421 5L5 6.41421L11.0104 12.4246L5 18.435L6.41421 19.8492L12.4246 13.8388L18.435 19.8492L19.8492 18.435L13.8388 12.4246L19.8492 6.41421L18.435 5L12.4246 11.0104L6.41421 5Z"
                                  fill="white"/>
                            <path d="M6.41421 5L5 6.41421L11.0104 12.4246L5 18.435L6.41421 19.8492L12.4246 13.8388L18.435 19.8492L19.8492 18.435L13.8388 12.4246L19.8492 6.41421L18.435 5L12.4246 11.0104L6.41421 5Z"
                                  fill="#2DCCD3"/>
                        </svg>
                    </div>
                    <div class="navigation__dropdown dropdown--onelevel">
                        <a class="navigation__link link--category" href="#">мебель</a>
                        <div class="navigation__dropdown dropdown--first">
                            <div class="dropdown__column"><a class="navigation__link link--category" href="#">Серия CLIC</a>
                                <a class="navigation__link" href="#">Столы</a>
                                <a class="navigation__link" href="#">Стулья</a>
                                <a class="navigation__link" href="#">Этажерка</a>
                                <a class="navigation__link" href="#">Стул для кормления</a>
                            </div>
                            <div class="dropdown__column">
                                <a class="navigation__link link--category" href="#">Серия MODE</a>
                                <a class="navigation__link" href="#">Кровати</a>
                                <a class="navigation__link" href="#">Шкафы</a>
                                <a class="navigation__link" href="#">Стеллажи</a>
                                <a class="navigation__link" href="#">Лабиринт</a>
                                <a class="navigation__link" href="#">Полки</a>
                                <a class="navigation__link" href="#">Лестницы</a>
                                <a class="navigation__link" href="#">Комод</a>
                                <a class="navigation__link" href="#">Столы</a>
                                <a class="navigation__link" href="#">Кубики</a>
                            </div>
                            <div class="dropdown__column"><a class="navigation__link link--category" href="#">Серия
                                    MOVE</a><a class="navigation__link" href="#">Столы</a><a class="navigation__link"
                                                                                             href="#">Стулья</a><a
                                        class="navigation__link" href="#">Этажерка</a><a class="navigation__link"
                                                                                         href="#">Стул для кормления</a>
                            </div>
                            <div class="dropdown__column"><a class="navigation__link link--category" href="#">Серия
                                    PLAY</a><a class="navigation__link" href="#">Кровати</a><a class="navigation__link"
                                                                                               href="#">Шкафы</a><a
                                        class="navigation__link" href="#">Стеллажи</a><a class="navigation__link"
                                                                                         href="#">Лабиринт</a><a
                                        class="navigation__link" href="#">Полки</a><a class="navigation__link" href="#">Лестницы</a><a
                                        class="navigation__link" href="#">Комод</a><a class="navigation__link" href="#">Столы</a><a
                                        class="navigation__link" href="#">Рукоход</a><a class="navigation__link"
                                                                                        href="#">Качели</a><a
                                        class="navigation__link" href="#">Кольца</a></div>
                        </div>
                    </div>
                    <a class="navigation__link link--category" href="#">аксессуары</a><a class="navigation__link"
                                                                                         href="#" js-teleport
                                                                                         data-teleport-to="navigation__project"
                                                                                         data-teleport-condition="&lt;768">проектирование</a><a
                            class="navigation__link" href="#" js-teleport data-teleport-to="navigation__cooperation"
                            data-teleport-condition="&lt;992">сотрудничество</a>
                    <div class="navigation__dropdown"><a class="navigation__link link--dropdown" href="#"><i
                                    class="icon-more"></i></a>
                        <div class="navigation__dropdown dropdown--first">
                            <div class="navigation__dropdown"><a class="navigation__link link--category" href="#">Аксессуары</a>
                                <div class="navigation__dropdown dropdown--second"><a class="navigation__link" href="#">игрушки</a><a
                                            class="navigation__link" href="#">свет</a>
                                    <div class="navigation__dropdown"><a class="navigation__link" href="#">серия Эко</a>
                                        <div class="navigation__dropdown dropdown--third"><a class="navigation__link"
                                                                                             href="#">Простыни</a><a
                                                    class="navigation__link" href="#">наволочки</a><a
                                                    class="navigation__link" href="#">Пододеяльники</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="navigation__dropdown dropdown--border"><a
                                        class="navigation__link link--category link--border" href="#">Матрасы</a>
                                <div class="navigation__dropdown dropdown--second"><a class="navigation__link" href="#">игрушки</a><a
                                            class="navigation__link" href="#">свет</a>
                                    <div class="navigation__dropdown"><a class="navigation__link" href="#">серия Эко</a>
                                        <div class="navigation__dropdown dropdown--third"><a class="navigation__link"
                                                                                             href="#">Простыни</a><a
                                                    class="navigation__link" href="#">наволочки</a><a
                                                    class="navigation__link" href="#">Пододеяльники</a></div>
                                    </div>
                                </div>
                            </div>
                            <a class="navigation__link" href="#" data-teleport-target="navigation__cooperation"></a><a
                                    class="navigation__link" href="#" data-teleport-target="navigation__project"></a><a
                                    class="navigation__link" href="#">О компании</a><a class="navigation__link"
                                                                                       href="#">Блог</a><a
                                    class="navigation__link" href="#">Доставка и оплата</a><a class="navigation__link"
                                                                                              href="#">Публичная
                                оферта</a><a class="navigation__link" href="#">Политика конфиденциальности</a><a
                                    class="navigation__link" href="#">Каталоги PDF</a></div>
                    </div>
                </nav>
            </div>
            <div class="col-auto col-md-1 order-md-last"><a class="header__search" href="#"><i class="icon-search"></i></a>
            </div>
            <div class="col-auto col-md-1 order-md-last">
                <div class="header__cart"><a class="cart__counter" href="#"> <i
                                class="icon-basket-empty"> </i><span>0</span></a>
                    <div class="cart__dropdown">
                        <div class="dropdown__item">
                            <div class="dropdown__item_image"><img class="img-responvise"
                                                                   src="<?= SITE_TEMPLATE_PATH ?>/assets/images/cart_image.png"></div>
                            <div class="dropdown__item_info">
                                <div class="dropdown__item_name">Этажерка WHATNOT</div>
                                <div class="dropdown__item_left"><span class="dropdown__item_vendor">WHCYL0</span><span
                                            class="dropdown__item_color">Желтый</span><span
                                            class="dropdown__item_hashtag"># 0</span><span
                                            class="dropdown__item_counts">1 шт</span></div>
                                <div class="dropdown__item_right"><span
                                            class="dropdown__item_price">5 500 Р.</span><span
                                            class="dropdown__item_price--old">5 500 Р.</span><span
                                            class="dropdown__item_delete">удалить</span></div>
                            </div>
                        </div>
                        <div class="dropdown__item">
                            <div class="dropdown__item_image"><img class="img-responvise"
                                                                   src="<?= SITE_TEMPLATE_PATH ?>/assets/images/cart_image.png"></div>
                            <div class="dropdown__item_info">
                                <div class="dropdown__item_name">Стул для кормления HIGHCHAIR</div>
                                <div class="dropdown__item_left"><span class="dropdown__item_vendor">WHCYL0</span><span
                                            class="dropdown__item_color">Желтый</span><span
                                            class="dropdown__item_hashtag">#</span><span class="dropdown__item_counts">1 шт</span>
                                </div>
                                <div class="dropdown__item_right"><span
                                            class="dropdown__item_price">19 000 Р.</span><span
                                            class="dropdown__item_price--old"></span><span
                                            class="dropdown__item_delete">удалить</span></div>
                            </div>
                        </div>
                        <div class="dropdown__cost">
                            <div class="dropdown__cost_title">Общая стоимость товаров<span>Без учета стоимости <a
                                            href="#">доставки</a></span></div>
                            <div class="dropdown__cost_total">24 500 Р.</div>
                        </div>
                        <div class="dropdown__buttons"><a class="btn" href="#">Оформить заказ</a></div>
                    </div>
                    <div class="cart__dropdown">
                        <div class="dropdown__added">товар добавлен в корзину</div>
                        <div class="dropdown__buttons"><a class="btn" href="#">Оформить заказ</a><a
                                    class="btn btn--open" href="#">Смотреть корзину (2)</a><a class="dropdown__continue"
                                                                                              href="#">Продолжить
                                покупки</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
