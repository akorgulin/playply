<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

class UsersListComponent extends CBitrixComponent
{
    const USER_GROUP = 2;

    public function executeComponent()
    {
        global $APPLICATION;
        $APPLICATION->SetTitle(Loc::getMessage('USERS_COMPONENT.PAGE_TITLE'));

        if(!$this->checkModules()) {
            return null;
        }

        $page = $this->initComponentPage();

        $result = UserTable::getList(array(
            'select' => array('*'),
            'order' => array('LAST_LOGIN'=>'DESC')
        ));

        while ($arUser = $result->fetch()) {
            $this->arResult["USERS"][] = $arUser;
        }

        $page->includeComponentTemplate($this->arResult["PAGE"]);

        return null;
    }

    protected function checkModules()
    {
        $modulesToCheck = array('iblock', 'main');
        $modulesInstalledFlag = true;

        foreach ($modulesToCheck as $module) {
            if(!\Bitrix\Main\Loader::includeModule($module)) {
                $modulesInstalledFlag = false;
                echo '<div class="msg msg-error">' . Loc::getMessage('USERS_COMPONENT.MODULE_NOT_INSTALLED', array('#MODULE_NAME#' => $module)) . '</div>';
            }
        }

        return $modulesInstalledFlag;
    }

    /**
     * @return $this
     * @throws \Bitrix\Main\SystemException
     */
    protected function initComponentPage()
    {
        $this->arResult['ERRORS'] = array();
        if ($this->arParams["SEF_MODE"] != "Y") {
            throw new \Bitrix\Main\SystemException(Loc::getMessage('USERS_COMPONENT.SEF_MODE_IS_MANDATORY'));
        }

        $arDefaultUrlTemplates404 = array(
            "list" => "contract/",
            "add" => "add/",
            "detail" => "contract/#CONTRACT#/",
        );

        $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);
        $arVariables = array();
        $page = CComponentEngine::ParseComponentPath($this->arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);

        $this->arResult = array(
            "SEF_FOLDER" => $this->arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $arVariables
        );

        $this->arResult["PAGE"] = (!$page) ? 'list' : $page;

        return $this;
    }
}