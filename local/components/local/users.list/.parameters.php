<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "SEF_MODE" => array(
            'list' => array(
                "NAME" => Loc::getMessage('USERS_COMP.PARAMS.CONTRACTS_LIST'),
                "DEFAULT" => "list/",
                "VARIABLES" => array(),
            ),
            'add' => array(
                "NAME" => Loc::getMessage('USERS_COMP.PARAMS.START_NEGOTIATING'),
                "DEFAULT" => "add/",
                "VARIABLES" => array(),
            ),
            'detail' => array(
                "NAME" => Loc::getMessage('USERS_COMP.PARAMS.CONTRACT_NEGOTIATION'),
                "DEFAULT" => "contract/#CONTRACT#/",
                "VARIABLES" => array('CONTRACT'),
            )
        )
    )
);