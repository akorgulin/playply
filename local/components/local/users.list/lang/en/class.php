<?php
$MESS['USERS_COMPONENT.PAGE_NOT_FOUND'] = 'The requested page was not found';
$MESS['USERS_COMPONENT.SEF_MODE_IS_MANDATORY'] = 'The component requires a SEF mode';
$MESS['USERS_COMPONENT.MODULE_NOT_INSTALLED'] = 'module # MODULE_NAME # is not included';
$MESS['USERS_COMPONENT.PAGE_TITLE'] = 'Users list';