<?php
$MESS['USERS_COMPONENT.PAGE_NOT_FOUND'] = 'Запрашиваемая страница не найдена';
$MESS['USERS_COMPONENT.SEF_MODE_IS_MANDATORY'] = 'Для работы компонента необходим режим ЧПУ';
$MESS['USERS_COMPONENT.MODULE_NOT_INSTALLED'] = 'модуль #MODULE_NAME# не подключен';
$MESS['USERS_COMPONENT.PAGE_TITLE'] = 'Список пользователей';