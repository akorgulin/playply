import $ from 'jquery';
import Swiper from 'swiper';

//global vars
var _window = $(window);

$(function () {
	init__slider_main();
	hover__product_image();
	delete__cart_item();
	cart__clickable();
	init__slider_lang();
	init__slider_product();
	initTeleport();
	initFixmenu();
	initFixHeaderMobile();
	initToggleMenu();

	//слайдер на главной
	function init__slider_main() {
		var slider__homepage = new Swiper('#slider__homepage', {
			pagination: {
				el: '.swiper-pagination',
				type: 'progressbar',
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			speed: 1500,
			loop: true,
			simulateTouch:false,
			autoplay: {
				delay: 5000 
			},
		});
	}

	//слайдер языков TODO - повесить на on change ajax на смену языка
	function init__slider_lang() {
		var slider__lang = new Swiper('#slider__lang', {
			direction: 'vertical',
			navigation: {
				nextEl: '#slider__lang .swiper-button-next',
				prevEl: '#slider__lang .swiper-button-prev',
			},
			speed: 1500,
			centeredSlides: true,
			slidesPerView: 3,
			slideToClickedSlide: true,
		});
		var slider__lang = new Swiper('#slider__lang-footer', {
			direction: 'vertical',
			navigation: {
				nextEl: '#slider__lang-footer .swiper-button-next',
				prevEl: '#slider__lang-footer .swiper-button-prev',
			},
			speed: 1500,
			centeredSlides: true,
			slidesPerView: 3,
			slideToClickedSlide: true,
		});
	}

	//init product sliders
	function init__slider_product() {
		if (_window.width() < 577) {			
			var slider__products = new Swiper('.slider__products', {
				speed: 1500,
				slidesPerView: 'auto'
			});
		}
	}

	//замена изображения при наведении на цвет
	function hover__product_image() {
		$('.product__color_block svg').hover(function() {
			var imagePosition = $(this).data('image');
			$(this).closest('.product__color').prev().find('img').css('display','none');
			$(this).closest('.product__color').prev().find('img:nth-child(' + imagePosition +')').css('display','block');
		}, function() {
			return
		});
	}

	//удаление товара с корзины. TODO - добавить ajax битрикса
	function delete__cart_item() {
		$('.dropdown__item_delete').on('click', function() {
			$(this).closest('.dropdown__item').fadeOut();
			
		});
	}

	//Функция для реализации хотелки про клики на корзину. TODO - изменить ссылку перехода на корректную 
	function cart__clickable() {
		var clickedOnce = false;
		var timer;
		$(".cart__counter").bind("click", function(){
			if (_window.width() > 767) {			
				if (clickedOnce) {
					run_on_double_click();
				} else {
					timer = setTimeout(run_on_simple_click, 300);
					clickedOnce = true;
				}
			} else {
				window.location.href = "/cart";
			}
		});

		function run_on_simple_click() {
			clickedOnce = false;
			if ($('.cart__counter').next().hasClass('opened')) {
				window.location.href = "/cart";
			} else {
				$('.cart__counter').next().addClass('opened');
			}
		}
		function run_on_double_click() {
			clickedOnce = false;
			clearTimeout(timer);
			window.location.href = "/cart";
		}
		$(document).mouseup(function (e){
			var div = $(".header__cart");
			if (!div.is(e.target)
				&& div.has(e.target).length === 0) {
				$('.cart__dropdown').removeClass('opened');
		}
	});
	}

	//Перенос контента по дата атрибутам. 
	function initTeleport(){
		$('[js-teleport]').each(function (i, val) {
			var self = $(val)
			var objHtml = $(val).html();
			var target = $('[data-teleport-target=' + $(val).data('teleport-to') + ']');
			var conditionMedia = $(val).data('teleport-condition').substring(1);
			var conditionPosition = $(val).data('teleport-condition').substring(0, 1);

			if (target && objHtml && conditionPosition) {

				function teleport() {
					var condition;

					if (conditionPosition === "<") {
						condition = _window.width() < conditionMedia;
					} else if (conditionPosition === ">") {
						condition = _window.width() > conditionMedia;
					}

					if (condition) {
						target.html(objHtml)
						self.html('').addClass('teleported')
					} else {
						self.html(objHtml).removeClass('teleported')
						target.html("")
					}
				}

				teleport();
				_window.on('resize', debounce(teleport, 100));


			}
		})
	}

	//Открываем\скрывает меню при клике на элемент с подменю
	function initFixmenu() {
		$('.navigation__dropdown > a').on('click', function() {
			if ($(this).hasClass('opened')) {
				$(this).removeClass('opened').parent().removeClass('opened');
			}  else {
				$(this).addClass('opened').parent().addClass('opened');
			}
		});
		$(document).mouseup(function (e){
			var div = $(".header__navigation > .navigation__dropdown");
			if (!div.is(e.target)
				&& div.has(e.target).length === 0) {
				$('.navigation__dropdown').removeClass('opened');
		}
	});
	}

	//fixed header on scroll
	function initFixHeaderMobile() {
		_window.scroll(function(){
			var sticky = $('header'),
			scroll = _window.scrollTop();

			if (scroll > 0) sticky.addClass('fixed');
			else sticky.removeClass('fixed');
		});
	}

	//toggle menu on mobile
	function initToggleMenu() {
		$('[js-menu]').on('click', function() {
			$('.header__navigation').toggleClass('opened');
			$('html').toggleClass('ovh');
		});
	}
});

'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function isObject(value) {
	var type = typeof value === 'undefined' ? 'undefined' : _typeof(value);
	return value != null && (type == 'object' || type == 'function');
}
function debounce(func, wait, options) {
	var lastArgs = void 0,
	lastThis = void 0,
	maxWait = void 0,
	result = void 0,
	timerId = void 0,
	lastCallTime = void 0;

	var lastInvokeTime = 0;
	var leading = false;
	var maxing = false;
	var trailing = true;

	if (typeof func != 'function') {
		throw new TypeError('Expected a function');
	}
	wait = +wait || 0;
	if (isObject(options)) {
		leading = !!options.leading;
		maxing = 'maxWait' in options;
		maxWait = maxing ? Math.max(+options.maxWait || 0, wait) : maxWait;
		trailing = 'trailing' in options ? !!options.trailing : trailing;
	}

	function invokeFunc(time) {
		var args = lastArgs;
		var thisArg = lastThis;

		lastArgs = lastThis = undefined;
		lastInvokeTime = time;
		result = func.apply(thisArg, args);
		return result;
	}

	function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
 }

 function remainingWait(time) {
 	var timeSinceLastCall = time - lastCallTime;
 	var timeSinceLastInvoke = time - lastInvokeTime;
 	var timeWaiting = wait - timeSinceLastCall;

 	return maxing ? Math.min(timeWaiting, maxWait - timeSinceLastInvoke) : timeWaiting;
 }

 function shouldInvoke(time) {
 	var timeSinceLastCall = time - lastCallTime;
 	var timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return lastCallTime === undefined || timeSinceLastCall >= wait || timeSinceLastCall < 0 || maxing && timeSinceLastInvoke >= maxWait;
 }

 function timerExpired() {
 	var time = Date.now();
 	if (shouldInvoke(time)) {
 		return trailingEdge(time);
 	}
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
 }

 function trailingEdge(time) {
 	timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
    	return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
 }

 function cancel() {
 	if (timerId !== undefined) {
 		clearTimeout(timerId);
 	}
 	lastInvokeTime = 0;
 	lastArgs = lastCallTime = lastThis = timerId = undefined;
 }

 function flush() {
 	return timerId === undefined ? result : trailingEdge(Date.now());
 }

 function pending() {
 	return timerId !== undefined;
 }

 function debounced() {
 	var time = Date.now();
 	var isInvoking = shouldInvoke(time);

 	for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
 		args[_key] = arguments[_key];
 	}

 	lastArgs = args;
 	lastThis = this;
 	lastCallTime = time;

 	if (isInvoking) {
 		if (timerId === undefined) {
 			return leadingEdge(lastCallTime);
 		}
 		if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
     }
  }
  if (timerId === undefined) {
  	timerId = setTimeout(timerExpired, wait);
  }
  return result;
}
debounced.cancel = cancel;
debounced.flush = flush;
debounced.pending = pending;
return debounced;
}
function throttle(func, wait, options) {
	var leading = true;
	var trailing = true;

	if (typeof func != 'function') {
		throw new TypeError('Expected a function');
	}
	if (isObject(options)) {
		leading = 'leading' in options ? !!options.leading : leading;
		trailing = 'trailing' in options ? !!options.trailing : trailing;
	}
	return debounce(func, wait, {
		'leading': leading,
		'maxWait': wait,
		'trailing': trailing
	});
}